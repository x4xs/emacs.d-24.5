;;; timestemp.el --- insert org mode style time stemps every ware (in every mode)

;; Keywords: timestemp

;;; Author: x4x
;;; at: <2014-12-13 Samstag 19:21>

;;; Code:
(defun insert-timestemp ()
  (interactive)
  (insert (format-time-string "<%Y-%m-%d %A %H:%M>"))
  )

(provide 'timestemp)
;;; timestemp.el ends here
