;;; init.el --- x4x's emacs config
;;; Author: x4x
;;; Location: ~/emacs.d/init.d

;;; Commentary:
;; x4x's second emacs config
;; 26.10.2016

;;; Code:

;; test for emacs version higher then 24
(let ((minver 24))
  (unless (>= emacs-major-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))

;; rise garbage collector activation memory threshold (in bytes) during init.
(setq gc-cons-threshold 400000000)

;; Turn off mouse interface early in startup to avoid momentary display
(menu-bar-mode -1)
(when window-system
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1))

;; disable start up page
(setq inhibit-startup-message t)
;; scratch buffer message
(setq initial-scratch-message "")

;; package manger list
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(when (boundp 'package-pinned-packages)
  (setq package-pinned-packages
'((org-plus-contrib . "org"))))
(package-initialize)

;;; Bootstrap use-package
;; Install use-package if it's not already installed.
;; use-package is used to configure the rest of the packages.
(unless (or (package-installed-p 'use-package)
            (package-installed-p 'diminish))
  (package-refresh-contents)
  (package-install 'use-package)
  (package-install 'diminish))

;; From use-package README
(eval-when-compile
  (require 'use-package))
(require 'diminish)                ;; if you use :diminish
(require 'bind-key)
;; (setq use-package-verbose t)
(require 'server)
(unless (server-running-p) (server-start))

;;; Load the config
(require 'org-install)  ;; might be required in old emacs versions
(require 'org)
(org-babel-load-file (concat user-emacs-directory "config.org"))

;; lower garbage collector activation memory threshold (in bytes) for normal use.
(setq gc-cons-threshold 800000)

;;; init.el ends here
(put 'scroll-left 'disabled nil)
