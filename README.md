# x4x's 2nd emacs config #
This is my second emacs config.
It is based on the org config methode from [Daniel Mai emacs configuration](https://github.com/danielmai/.emacs.d).

'init.el' only laods absolutly nesesery things.
*all actual configuration is done in 'config.org'.*

## Downlaod it

```bash
git clone https://bitbucket.org/x4xs/emacs.d-24.5.git --recursive
mv emacs.d-24.5 ~/.emacs.d
#emacs --debug-init  &
```

## Directories

| dir         | description                          |   |
|-------------+--------------------------------------+---|
| ./modules   | manual installed,single module files |   |
| ./packages  | manual installed, packages(folders)  |   |
| ./elpa      | elpa packages (automatic created)    |   |
| ./snippets  | yasnippets                           |   |
| ./templates | folder for templates                 |   |
